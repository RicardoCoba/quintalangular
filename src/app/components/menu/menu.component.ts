import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-menu',
  templateUrl: './menu.component.html',
  styleUrls: ['./menu.component.css']
})
export class MenuComponent implements OnInit {


  rutas = [{
    name: 'home',
    path: '/home'
    },
    {
    name: 'about',
    path: '/about'
    },
    {
      name: 'post',
      path: '/post'
      },
    {
    name: 'contact',
    path: '/contact'
}];

  constructor() { }

  ngOnInit() {
  }

}
